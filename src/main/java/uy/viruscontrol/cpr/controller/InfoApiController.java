package uy.viruscontrol.cpr.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import uy.viruscontrol.cpr.model.*;
import uy.viruscontrol.cpr.persistence.Database;
import uy.viruscontrol.cpr.service.InfoApiService;

@RestController
@Slf4j
public class InfoApiController implements InfoApiService {
  private final Database database;

  @Autowired
  public InfoApiController(Database database) {
    this.database = database;
  }

  public ResponseEntity<ResourceProvider> getServerInformation() {
    ResourceProvider info = database.getInfo();
    if (info == null) {
      throw new ResponseStatusException(
        HttpStatus.BAD_REQUEST,
        "Servidor no configurado papa."
      );
    }
    return ResponseEntity.ok().body(info);
  }

  public ResponseEntity<ResourceProvider> setServerInformation(
    ResourceProvider info
  ) {
    database.setInfo(info);
    return ResponseEntity.ok().body(info);
  }
}
