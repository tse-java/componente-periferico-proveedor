package uy.viruscontrol.cpr.controller;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import uy.viruscontrol.cpr.model.CentralNode;
import uy.viruscontrol.cpr.persistence.Database;
import uy.viruscontrol.cpr.service.RegisterApiService;

@RestController
@Slf4j
public class RegisterController implements RegisterApiService {
  private final Database database;

  @Autowired
  public RegisterController(Database database) {
    this.database = database;
  }

  @Override
  public ResponseEntity<CentralNode> register(CentralNode node) {
    node = database.addCentralNode(node);
    log.info("Nuevo nodo central: " + node.toString());
    return ResponseEntity.ok().body(node);
  }

  @Override
  public ResponseEntity<List<CentralNode>> getNodes() {
    return ResponseEntity.ok().body(database.getNodes());
  }
}
