package uy.viruscontrol.cpr.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import uy.viruscontrol.cpr.model.*;
import uy.viruscontrol.cpr.persistence.Database;
import uy.viruscontrol.cpr.service.ResourcesApiService;

@RestController
@Slf4j
public class ResourceController implements ResourcesApiService {
  private final Database database;

  @Autowired
  public ResourceController(Database database) {
    this.database = database;
  }

  private final ObjectMapper objectMapper = new ObjectMapper();

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public ResponseEntity<List<ResourceQuantity>> getResources(int nodeId) {
    CentralNode node = database.getNode(nodeId);
    if (node == null) {
      return ResponseEntity.notFound().build();
    }
    OkHttpClient client = new OkHttpClient();
    Request request = new Request.Builder()
      .url(node.getGetResourcesUrl())
      .addHeader("Authorization", "Bearer " + node.getToken())
      .build();

    try {
      Response response = client.newCall(request).execute();
      if (response.code() == 200) {
        List<Object> objectList = objectMapper.readValue(
          response.body().string(),
          List.class
        );
        List<ResourceQuantity> resourceQuantityList = new ArrayList<>();
        for (Object o : objectList) {
          DTOBasicResource resource = modelMapper.map(
            o,
            DTOBasicResource.class
          );
          ResourceQuantity resourceQuantity = new ResourceQuantity();
          resourceQuantity.setResourceId(resource.getId());
          resourceQuantityList.add(resourceQuantity);
        }
        return ResponseEntity.ok().body(resourceQuantityList);
      }
      log.error(
        "Error. Codigo: [{}], Mensaje: [{}], url: [{}]",
        response.code(),
        response.body(),
        node.getGetResourcesUrl()
      );
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    } catch (IOException e) {
      log.error(" ERROR: {}", e.getMessage());
      return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }
  }

  @Override
  public ResponseEntity<List<DTOWrongId>> setServerInformation(
    int nodeId,
    List<ResourceQuantity> resourceQuantityList
  ) {
    CentralNode node = database.getNode(nodeId);
    if (node == null) {
      return ResponseEntity.notFound().build();
    }
    OkHttpClient client = new OkHttpClient();
    Request request = null;
    try {
      request =
        new Request.Builder()
          .url(node.getSetAvailabilityUrl())
          .addHeader("Authorization", "Bearer " + node.getToken())
          .post(
            RequestBody.create(
              MediaType.parse("application/json"),
              objectMapper.writeValueAsString(resourceQuantityList)
            )
          )
          .build();
    } catch (JsonProcessingException e) {
      log.error("Error procesando Json");
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    try {
      Response response = client.newCall(request).execute();
      if (response.code() == 200) {
        List<Object> objectList = objectMapper.readValue(
          response.body().string(),
          List.class
        );
        List<DTOWrongId> wrongIds = new ArrayList<>();
        for (Object o : objectList) {
          DTOWrongId wrongId = modelMapper.map(o, DTOWrongId.class);
          wrongIds.add(wrongId);
        }
        return ResponseEntity.ok().body(wrongIds);
      }
      log.error(
        "Error. Codigo: [{}], Mensaje: [{}], url: [{}]",
        response.code(),
        response.body(),
        node.getGetResourcesUrl()
      );
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    } catch (IOException e) {
      log.error(" ERROR: {}", e.getMessage());
      return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }
  }
}
