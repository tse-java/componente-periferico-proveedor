package uy.viruscontrol.cpr.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uy.viruscontrol.cpr.model.*;
import uy.viruscontrol.cpr.persistence.Database;

@Controller
@Slf4j
public class GuiService {
  private final Database database;

  private final ObjectMapper objectMapper = new ObjectMapper();
  private final ModelMapper modelMapper = new ModelMapper();

  @Autowired
  public GuiService(Database database) {
    this.database = database;
  }

  @GetMapping(value = "/gui")
  public String getNodes(
    @RequestParam(required = false, defaultValue = "") String centralNode,
    @RequestParam(required = false, defaultValue = "") String msg,
    Model model
  ) {
    if (!centralNode.isEmpty()) {
      model.addAttribute(
        "resources",
        getResources(Integer.valueOf(centralNode))
      );
      model.addAttribute(
        "selectedNode",
        database.getNode(Integer.valueOf(centralNode))
      );
    } else {
      model.addAttribute("resources", new ArrayList<>());
      model.addAttribute("selectedNode", null);
    }
    model.addAttribute("isConfigured", database.getInfo() != null);
    model.addAttribute("nodes", database.getNodes());
    model.addAttribute("msg", msg);
    return "list";
  }

  private List<DTOBasicResource> getResources(Integer id) {
    List<DTOBasicResource> resources = new ArrayList<>();
    CentralNode node = database.getNode(id);
    if (node != null) {
      OkHttpClient client = new OkHttpClient();
      Request request = new Request.Builder()
        .url(node.getGetResourcesUrl())
        .addHeader("Authorization", "Bearer " + node.getToken())
        .build();
      try {
        Response response = client.newCall(request).execute();
        if (response.code() == 200) {
          List<Object> objectList = objectMapper.readValue(
            response.body().string(),
            List.class
          );
          List<ResourceQuantity> resourceQuantityList = new ArrayList<>();
          for (Object o : objectList) {
            DTOBasicResource resource = modelMapper.map(
              o,
              DTOBasicResource.class
            );
            resources.add(resource);
          }
        }
        log.error(
          "Error. Codigo: [{}], Mensaje: [{}], url: [{}]",
          response.code(),
          response.body(),
          node.getGetResourcesUrl()
        );
      } catch (IOException e) {
        log.error(" ERROR: {}", e.getMessage());
      }
    }
    return resources;
  }

  @PostMapping(value = "/gui/quantity")
  public String sendQuantities(
    @RequestParam(value = "quantities[]") String[] quantities,
    @RequestParam(value = "ids[]") String[] ids,
    @RequestParam Integer nodeId,
    Model model
  ) {
    List<ResourceQuantity> resourceQuantityList = new ArrayList<>();
    for (int i = 0; i < quantities.length; i++) {
      if (!quantities[i].isEmpty()) {
        ResourceQuantity resourceQuantity = new ResourceQuantity();
        resourceQuantity.setResourceId(Long.valueOf(ids[i]));
        resourceQuantity.setQuantity(Long.valueOf(quantities[i]));
        resourceQuantityList.add(resourceQuantity);
      }
    }
    CentralNode node = database.getNode(nodeId);
    if (node != null) {
      List<DTOWrongId> wrongIds = setAvailabilities(node, resourceQuantityList);
      if (wrongIds == null) {
        return "redirect:/gui?msg=error";
      }
      return "redirect:/gui?msg=ok";
    }
    return "redirect:/gui?msg=error";
  }

  private List<DTOWrongId> setAvailabilities(
    CentralNode node,
    List<ResourceQuantity> resourceQuantityList
  ) {
    OkHttpClient client = new OkHttpClient();
    Request request = null;
    try {
      request =
        new Request.Builder()
          .url(node.getSetAvailabilityUrl())
          .addHeader("Authorization", "Bearer " + node.getToken())
          .post(
            RequestBody.create(
              MediaType.parse("application/json"),
              objectMapper.writeValueAsString(resourceQuantityList)
            )
          )
          .build();
    } catch (JsonProcessingException e) {
      log.error("Error procesando Json");
    }

    try {
      Response response = client.newCall(request).execute();
      if (response.code() == 200) {
        List<Object> objectList = objectMapper.readValue(
          response.body().string(),
          List.class
        );
        List<DTOWrongId> wrongIds = new ArrayList<>();
        for (Object o : objectList) {
          DTOWrongId wrongId = modelMapper.map(o, DTOWrongId.class);
          wrongIds.add(wrongId);
        }
        return wrongIds;
      }
      log.error(
        "Error. Codigo: [{}], Mensaje: [{}], url: [{}]",
        response.code(),
        response.body(),
        node.getGetResourcesUrl()
      );
      return null;
    } catch (IOException e) {
      log.error(" ERROR: {}", e.getMessage());
      return null;
    }
  }

  @GetMapping(value = "gui/config")
  public String configForm(Model model) {
    model.addAttribute("departments", UruguayanDepartment.values());
    return "config";
  }

  @PostMapping(value = "gui/config")
  public String config(
    String name,
    String street,
    String town,
    String city,
    String department,
    Model model
  ) {
    ResourceProvider provider = new ResourceProvider();
    provider.setCommercialName(name);
    Location location = new Location();
    location.setStreet(street);
    location.setTown(town);
    location.setCity(city);
    location.setDepartment(UruguayanDepartment.valueOf(department));
    provider.setAddress(location);
    List<OpenScheduleDay> openScheduleDays = new ArrayList<>();
    for (Day day : Day.values()) {
      OpenScheduleDay scheduleDay = new OpenScheduleDay();
      scheduleDay.setOpeningHour("10:00");
      scheduleDay.setClosingHour("22:00");
      scheduleDay.setDayOfWeek(day);
      openScheduleDays.add(scheduleDay);
    }
    provider.setOpenSchedule(openScheduleDays);
    database.setInfo(provider);
    return "redirect:/gui";
  }
}
