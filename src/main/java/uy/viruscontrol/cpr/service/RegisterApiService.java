package uy.viruscontrol.cpr.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uy.viruscontrol.cpr.model.CentralNode;

@RequestMapping(value = "/register", produces = "application/json")
@Tag(name = "Register")
public interface RegisterApiService {
  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  @Operation(description = "Register central node")
  ResponseEntity<CentralNode> register(@RequestBody CentralNode node);

  @GetMapping
  @ResponseStatus(code = HttpStatus.OK)
  @Operation(description = "return all registered central nodes")
  ResponseEntity<List<CentralNode>> getNodes();
}
