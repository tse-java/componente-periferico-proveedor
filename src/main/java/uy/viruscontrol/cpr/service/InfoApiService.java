package uy.viruscontrol.cpr.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uy.viruscontrol.cpr.model.ResourceProvider;

@RequestMapping(value = "/provider")
@Tag(name = "Server Info")
public interface InfoApiService {
  @GetMapping
  @Operation(description = "Get server info.")
  ResponseEntity<ResourceProvider> getServerInformation();

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  @Operation(description = "Setup server")
  ResponseEntity<ResourceProvider> setServerInformation(
    @RequestBody ResourceProvider dtoExamRequest
  );
}
