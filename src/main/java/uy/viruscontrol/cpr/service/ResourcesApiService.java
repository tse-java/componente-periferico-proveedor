package uy.viruscontrol.cpr.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uy.viruscontrol.cpr.model.DTOWrongId;
import uy.viruscontrol.cpr.model.ResourceQuantity;

@RequestMapping(value = "/resources", produces = "application/json")
@Tag(name = "Resources")
public interface ResourcesApiService {
  @GetMapping
  @ResponseStatus(code = HttpStatus.OK)
  @Operation(description = "return all resources from node")
  ResponseEntity<List<ResourceQuantity>> getResources(@RequestParam int nodeId);

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  @Operation(description = "Set resource on node ")
  ResponseEntity<List<DTOWrongId>> setServerInformation(
    @RequestParam int nodeId,
    @RequestBody List<ResourceQuantity> resourceQuantityList
  );
}
