package uy.viruscontrol.cpr.persistence;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import uy.viruscontrol.cpr.model.CentralNode;
import uy.viruscontrol.cpr.model.ResourceProvider;

@Configuration
public class Database {

  @Bean
  @Scope("singleton")
  public Database getDatabase() {
    return new Database();
  }

  private List<CentralNode> nodes = new ArrayList<>();
  private ResourceProvider info = null;
  private int nextId = 0;

  public CentralNode addCentralNode(CentralNode node) {
    node.setId(nextId++);
    node.setRegisteredAt(LocalDateTime.now());
    nodes.add(node);
    return node;
  }

  public List<CentralNode> getNodes() {
    return nodes;
  }

  public CentralNode getNode(int id) {
    for (CentralNode node : nodes) {
      if (node.getId() == id) return node;
    }
    return null;
  }

  public ResourceProvider getInfo() {
    return info;
  }

  public void setInfo(ResourceProvider info) {
    this.info = info;
    System.out.println("Se cambio la informacion del proveedor");
    System.out.println(this.info.toString());
    System.out.println("------------");
  }
}
