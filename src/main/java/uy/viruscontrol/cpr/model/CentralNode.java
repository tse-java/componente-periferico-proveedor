package uy.viruscontrol.cpr.model;

import java.time.LocalDateTime;

public class CentralNode {
  private int id;

  private String setAvailabilityUrl;

  private String getResourcesUrl;

  private String token;

  private LocalDateTime registeredAt;

  public CentralNode() {}

  public String getSetAvailabilityUrl() {
    return setAvailabilityUrl;
  }

  public void setSetAvailabilityUrl(String setAvailabilityUrl) {
    this.setAvailabilityUrl = setAvailabilityUrl;
  }

  public String getGetResourcesUrl() {
    return getResourcesUrl;
  }

  public void setGetResourcesUrl(String getResourcesUrl) {
    this.getResourcesUrl = getResourcesUrl;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public LocalDateTime getRegisteredAt() {
    return registeredAt;
  }

  public void setRegisteredAt(LocalDateTime registeredAt) {
    this.registeredAt = registeredAt;
  }

  @Override
  public String toString() {
    return (
      "CentralNode{" +
      "id=" +
      id +
      ", setAvailabilityUrl='" +
      setAvailabilityUrl +
      '\'' +
      ", getResourcesUrl='" +
      getResourcesUrl +
      '\'' +
      ", token='" +
      token +
      '\'' +
      ", registeredAt=" +
      registeredAt +
      '}'
    );
  }
}
