package uy.viruscontrol.cpr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class ResourceProvider implements Serializable {
  private String commercialName;

  private Location address;

  private List<OpenScheduleDay> openSchedule = new ArrayList<>();
}
