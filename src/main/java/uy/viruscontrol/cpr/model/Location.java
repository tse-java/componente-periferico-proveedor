package uy.viruscontrol.cpr.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class Location implements Serializable {
  private String coordinates;

  private String street;

  private String town;

  private String city;

  private UruguayanDepartment department;
}
