package uy.viruscontrol.cpr.model;

public class ResourceQuantity {
  private Long resourceId;

  private Long quantity;

  public ResourceQuantity() {}

  public Long getResourceId() {
    return resourceId;
  }

  public void setResourceId(Long resourceId) {
    this.resourceId = resourceId;
  }

  public Long getQuantity() {
    return quantity;
  }

  public void setQuantity(Long quantity) {
    this.quantity = quantity;
  }
}
