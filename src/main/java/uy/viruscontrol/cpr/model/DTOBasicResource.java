package uy.viruscontrol.cpr.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class DTOBasicResource implements Serializable {
  private Long id;

  private String name;
}
