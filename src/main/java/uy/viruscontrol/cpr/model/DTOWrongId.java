package uy.viruscontrol.cpr.model;

import lombok.Data;

@Data
public class DTOWrongId {
  private Long wrongId;
}
