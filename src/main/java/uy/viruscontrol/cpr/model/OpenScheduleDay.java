package uy.viruscontrol.cpr.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class OpenScheduleDay implements Serializable {
  private Day dayOfWeek;

  private String openingHour;

  private String closingHour;
}
